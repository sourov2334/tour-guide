package com.example.souro.tourguide;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main2Activity extends AppCompatActivity {

    private ListView listView;
    DatabaseHelper myDbHelper;
    String district;
    List<String> dataLis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setElevation(0);
        actionBar.setDisplayHomeAsUpEnabled(true);


        district = null;
        Intent i = getIntent();
        district = i.getStringExtra("district");
        actionBar.setTitle(district);
        myDbHelper = new DatabaseHelper(getApplicationContext());
        listView = (ListView) findViewById(R.id.listView2);

        dataLis = new ArrayList<>();
        dataLis = myDbHelper.getData1(district);
//        Log.d("ddddd", "dis: " + district);
//        Log.d("ddddd", String.valueOf(dataLis.size()));
//        Log.d("ddddd","list :"+dataLis.get(0));

        if (dataLis.size() == 1 && compare(dataLis.get(0),null)) {

        Toast.makeText(Main2Activity.this, "এই জেলার দর্শনীয় স্থান এখনো যুক্ত হয় নি", Toast.LENGTH_SHORT).show();
        } else {

            ArrayAdapter<String> adapter = new ArrayAdapter<>(Main2Activity.this, R.layout.sampleview, R.id.sampletextView1, dataLis);
            listView.setAdapter(adapter);
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i=new Intent(getApplicationContext(),Main3Activity.class);
                i.putExtra("place",dataLis.get(position));
                startActivity(i);
            }
        });
//            Toast.makeText(Main2Activity.this, "দর্শনীয় স্থান এখনো যুক্ত হয় নি এই জেলার", Toast.LENGTH_SHORT).show();


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
//                Toast.makeText(getApplicationContext(),"Back button clicked", Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }
    public static boolean compare(String str1, String str2) {
        return (str1 == null ? str2 == null : str1.equals(str2));
    }
}
