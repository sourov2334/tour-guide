package com.example.souro.tourguide;

import android.content.Intent;
import android.database.SQLException;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private ListView listView;
    DatabaseHelper myDbHelper;
    String division;
    List<String> dataLis;
    boolean doubleBackToExitPressedOnce = false;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


                myDbHelper=new DatabaseHelper(getApplicationContext());
        try {
            myDbHelper.createDataBase();
//            Toast.makeText(MainActivitya.this, "Create Success", Toast.LENGTH_SHORT).show();
            Log.d("xxx ", "createDataBase(): success");
        } catch (IOException ioe) {
            Log.d("xxx ", "createDataBase(): failed");
            throw new Error("Unable to create database");
        }
        try {
            myDbHelper.openDataBase();
            Log.d("xxx ", "openDataBase(): success");
//            Toast.makeText(MainActivitya.this, "open Success", Toast.LENGTH_SHORT).show();
        } catch (SQLException sqle) {
            Log.d("xxx ", "openDataBase(): faild");
            throw sqle;
        }

      //  FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        //fab.setOnClickListener(new View.OnClickListener() {
          //  @Override
            //public void onClick(View view) {
              //  Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
           // }
       // });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        listView =(ListView) findViewById(R.id.listView1);


        navigationView.getMenu().getItem(0).setChecked(true);
        drawer.openDrawer(GravityCompat.START);

        division="ঢাকা";
        dataLis=new ArrayList<>();
        dataLis=myDbHelper.getData(division);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this,R.layout.sampleview,R.id.sampletextView1,dataLis);
        listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i=new Intent(getApplicationContext(),Main2Activity.class);
                    i.putExtra("district",dataLis.get(position));
                    startActivity(i);
                }
            });



    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit \nfor Division Click on left corner", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_dhaka) {
//            String[] Dhaka = getResources().getStringArray(R.array.dhaka);
            division="ঢাকা";
            dataLis=myDbHelper.getData(division);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this,R.layout.sampleview,R.id.sampletextView1,dataLis);
            listView.setAdapter(adapter);

            // Handle the camera action
        } else if (id == R.id.nav_chittagong) {
            division="চট্টগ্রাম";
            dataLis=myDbHelper.getData(division);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this,R.layout.sampleview,R.id.sampletextView1,dataLis);
            listView.setAdapter(adapter);
        }

        else if (id == R.id.nav_khulna) {
            division="খুলনা";
            dataLis=myDbHelper.getData(division);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this,R.layout.sampleview,R.id.sampletextView1,dataLis);
            listView.setAdapter(adapter);
        }
        else if (id == R.id.nav_sylhet) {
            division="সিলেট";
            dataLis=myDbHelper.getData(division);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this,R.layout.sampleview,R.id.sampletextView1,dataLis);
            listView.setAdapter(adapter);
        }
        else if (id == R.id.nav_rajshahi) {
            division="রাজশাহী";
            dataLis=myDbHelper.getData(division);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this,R.layout.sampleview,R.id.sampletextView1,dataLis);
            listView.setAdapter(adapter);
        }
        else if (id == R.id.nav_rangpur) {
            division="রংপুর";
            dataLis=myDbHelper.getData(division);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this,R.layout.sampleview,R.id.sampletextView1,dataLis);
            listView.setAdapter(adapter);
        }
        else if (id == R.id.nav_barisal) {
            division="বরিশাল";
            dataLis=myDbHelper.getData(division);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this,R.layout.sampleview,R.id.sampletextView1,dataLis);
            listView.setAdapter(adapter);
        }
        else if (id == R.id.nav_mymensingh) {
            division="ময়মনসিংহ";
            dataLis=myDbHelper.getData(division);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this,R.layout.sampleview,R.id.sampletextView1,dataLis);
            listView.setAdapter(adapter);
        }
        else if (id == R.id.nav_about) {

            Intent i=new Intent(getApplicationContext(),about.class);
            startActivity(i);


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
