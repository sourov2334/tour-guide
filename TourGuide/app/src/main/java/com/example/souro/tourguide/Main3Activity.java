package com.example.souro.tourguide;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Main3Activity extends AppCompatActivity {
    private TextView textView;
    DatabaseHelper myDbHelper;
    String place;
    String dataLis;
    String picture;
   private ImageView imageView;
    Timer timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
       timer = new Timer();



        fab.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

//               ConnectivityManager cManager = (ConnectivityManager) getSystemService(Main3Activity.CONNECTIVITY_SERVICE);
//               final NetworkInfo nInfo = cManager.getActiveNetworkInfo();

//               if(nInfo!=null && nInfo.isConnected()){

               if (isConnected()){

//                   timer.schedule(new TimerTask() {
//                       @Override
//                       public void run() {
//                           Intent i=new Intent(getApplicationContext(),MapsActivity.class);
//                           i.putExtra("place",place);
//                           startActivity(i);
//                       }
//                   },5000);
                   Intent i=new Intent(getApplicationContext(),MapsActivity.class);
                   i.putExtra("place",place);
                   startActivity(i);
               }
               else{
                   Snackbar.make(view, "Please Turn On Internet", Snackbar.LENGTH_SHORT)
                           .setAction("Action", null)
                           .show();
               }


           }
        });

        place = null;
        Intent i = getIntent();
        place = i.getStringExtra("place");
        actionBar.setTitle(place);
        myDbHelper = new DatabaseHelper(getApplicationContext());
        textView =(TextView) findViewById(R.id.sampletextView2);
       imageView = (ImageView) findViewById(R.id.placeid1);


//        Log.d()


        dataLis =  null;
    //    picture = myDbHelper.getData3(place);
        dataLis = myDbHelper.getData2(place);
        picture = myDbHelper.getData3(place);

        Log.d("mmm","data: "+picture);




       // Log.d("mmm","count: "+dataLis.length());

        if  (compare(dataLis,null) ){
            Toast.makeText(Main3Activity.this, "বিবরণ  যুক্ত হয় নি", Toast.LENGTH_SHORT).show();
        }
        else {
            if(!compare(picture,null)){
                imageView.setVisibility(View.VISIBLE);
                String uri = "@drawable/"+picture;  // where myresource (without the extension) is the file
                Log.d("ddd","count: "+dataLis.length());
                int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                Drawable res = getResources().getDrawable(imageResource);
                imageView.setImageDrawable(res);
            }


            textView.setText(dataLis);

        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
//                Toast.makeText(getApplicationContext(),"Back button clicked", Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }

    public static boolean compare(String str1, String str2) {
        return (str1 == null ? str2 == null : str1.equals(str2));
    }

    public boolean isConnected(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;

        return connected;
    }

}